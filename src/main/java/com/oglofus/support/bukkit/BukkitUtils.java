package com.oglofus.support.bukkit;

import com.google.common.reflect.TypeToken;
import ninja.leaping.configurate.ConfigurationNode;
import ninja.leaping.configurate.objectmapping.ObjectMappingException;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializer;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializerCollection;
import ninja.leaping.configurate.objectmapping.serialize.TypeSerializers;
import org.bukkit.*;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Map;
import java.util.UUID;

/**
 * This file is part of Oglofus Support project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 05/06/2017.
 */
public final class BukkitUtils {
    public static final TypeSerializerCollection SERIALIZER = TypeSerializers
            .getDefaultSerializers()
            .newChild();

    static {
        SERIALIZER.registerType(TypeToken.of(OfflinePlayer.class), new OfflinePlayerSerializer());
        SERIALIZER.registerType(TypeToken.of(ItemStack.class), new ItemStackSerializer());
        SERIALIZER.registerType(TypeToken.of(Material.class), new MaterialSerializer());
        SERIALIZER.registerType(TypeToken.of(Location.class), new LocationSerialize());
        SERIALIZER.registerType(TypeToken.of(World.class), new WorldSerializer());
    }

    public static class LocationSerialize implements TypeSerializer<Location> {

        /**
         * Deserialize an object required to be of a given type from the given configuration node
         *
         * @param type  The type of return value required
         * @param value The node containing serialized data
         * @return An object
         * @throws ObjectMappingException If the presented data is somehow invalid
         */
        @Override
        public Location deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {

            if (value.getNode("world").isVirtual() ||
                    value.getNode("x").isVirtual() ||
                    value.getNode("y").isVirtual() ||
                    value.getNode("z").isVirtual()) {
                throw new ObjectMappingException();
            }

            World  world = value.getNode("world").getValue(TypeToken.of(World.class));
            Double x     = value.getNode("x").getDouble();
            Double y     = value.getNode("y").getDouble();
            Double z     = value.getNode("z").getDouble();
            Float  yaw   = value.getNode("yaw").getFloat(0.0F);
            Float  pitch = value.getNode("pitch").getFloat(0.0F);

            return new Location(world, x, y, z, yaw, pitch);
        }

        @Override
        public void serialize(TypeToken<?> type, Location obj, ConfigurationNode value) throws ObjectMappingException {

            value.getNode("world").setValue(obj.getWorld());
            value.getNode("x").setValue(obj.getX());
            value.getNode("y").setValue(obj.getX());
            value.getNode("z").setValue(obj.getZ());
            value.getNode("yaw").setValue(obj.getYaw());
            value.getNode("pitch").setValue(obj.getPitch());
        }
    }

    public static class ItemStackSerializer implements TypeSerializer<ItemStack> {

        /**
         * Deserialize an object required to be of a given type from the given configuration node
         *
         * @param type  The type of return value required
         * @param value The node containing serialized data
         * @return An object
         * @throws ObjectMappingException If the presented data is somehow invalid
         */
        @Override
        public ItemStack deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
            try {
                Material material = Material.getMaterial(value.getNode("type").getString());

                short damage = 0;
                int   amount = 1;

                if (!value.getNode("damage").isVirtual()) {
                    damage = (short) value.getNode("damage").getInt();
                }

                if (!value.getNode("amount").isVirtual()) {
                    amount = value.getNode("damage").getInt();
                }

                ItemStack result = new ItemStack(material, amount, damage);
                Object    raw;
                if (!value.getNode("enchantments").isVirtual()) {
                    raw = value.getNode("enchantments").getValue();

                    if (raw instanceof Map) {
                        Map<?, ?> map = (Map) raw;

                        for (Object o : map.entrySet()) {
                            Map.Entry<?, ?> entry       = (Map.Entry) o;
                            Enchantment     enchantment = Enchantment.getByName(entry.getKey().toString());
                            if (enchantment != null && entry.getValue() instanceof Integer) {
                                result.addUnsafeEnchantment(enchantment, (Integer) entry.getValue());
                            }
                        }
                    }
                } else if (!value.getNode("meta").isVirtual()) {
                    raw = value.getNode("meta").getValue();
                    if (raw instanceof ItemMeta) {
                        result.setItemMeta((ItemMeta) raw);
                    }
                }

                return result;
            } catch (Exception e) {
                throw new ObjectMappingException(e);
            }
        }

        @Override
        public void serialize(TypeToken<?> type, ItemStack obj, ConfigurationNode value) throws ObjectMappingException {
            value.setValue(obj.serialize());
        }
    }

    public static class MaterialSerializer implements TypeSerializer<Material> {

        /**
         * Deserialize an object required to be of a given type from the given configuration node
         *
         * @param type  The type of return value required
         * @param value The node containing serialized data
         * @return An object
         * @throws ObjectMappingException If the presented data is somehow invalid
         */
        @Override
        public Material deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {

            String name = value.getString();

            if (name == null) {
                throw new ObjectMappingException();
            }

            Material returned = Material.getMaterial(name);

            if (returned == null) {
                throw new ObjectMappingException();
            }

            return returned;
        }

        @Override
        public void serialize(TypeToken<?> type, Material obj, ConfigurationNode value) throws ObjectMappingException {
            value.setValue(obj.name());
        }
    }

    public static class OfflinePlayerSerializer implements TypeSerializer<OfflinePlayer> {

        /**
         * Deserialize an object required to be of a given type from the given configuration node
         *
         * @param type  The type of return value required
         * @param value The node containing serialized data
         * @return An object
         * @throws ObjectMappingException If the presented data is somehow invalid
         */
        @Override
        public OfflinePlayer deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
            UUID uuid = value.getValue(TypeToken.of(UUID.class));

            if (uuid == null) {
                throw new ObjectMappingException();
            }

            OfflinePlayer returned = Bukkit.getOfflinePlayer(uuid);

            if (returned == null) {
                throw new ObjectMappingException();
            }

            return returned;
        }

        @Override
        public void serialize(TypeToken<?> type, OfflinePlayer obj, ConfigurationNode value) throws
                ObjectMappingException {
            value.setValue(obj.getUniqueId());
        }
    }

    public static class WorldSerializer implements TypeSerializer<World> {

        /**
         * Deserialize an object required to be of a given type from the given configuration node
         *
         * @param type  The type of return value required
         * @param value The node containing serialized data
         * @return An object
         * @throws ObjectMappingException If the presented data is somehow invalid
         */
        @Override
        public World deserialize(TypeToken<?> type, ConfigurationNode value) throws ObjectMappingException {
            UUID uuid = value.getValue(TypeToken.of(UUID.class));

            if (uuid == null) {
                throw new ObjectMappingException();
            }

            World returned = Bukkit.getWorld(uuid);

            if (returned == null) {
                throw new ObjectMappingException();
            }

            return returned;
        }

        @Override
        public void serialize(TypeToken<?> type, World obj, ConfigurationNode value) throws
                ObjectMappingException {
            value.setValue(obj.getUID());
        }
    }
}
