package com.oglofus.support.bukkit.api.menu;

import com.google.common.collect.Maps;
import org.apache.commons.lang.Validate;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import java.util.Map;

/**
 * This file is part of Oglofus Store project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 17/06/2017.
 */
public interface Menu extends Listener {
    static Builder builder(Plugin plugin) {
        return new Builder(plugin);
    }

    String getName();

    Integer getSize();

    Map<Integer, ItemStack> getStacks();

    Map<Integer, ClickHandler> getHandlers();

    void registerStack(Integer index, ItemStack stack);

    void registerHandler(Integer index, ClickHandler handler);

    void removeStack(Integer index);

    void removeHandler(Integer index);

    Inventory createInventory(InventoryHolder holder);

    Inventory updateInventory(Inventory inventory);

    void destroy();

    interface ClickHandler {
        boolean execute(ClickEvent event);
    }

    class ClickEvent {
        private final InventoryClickEvent event;
        private boolean close   = false;
        private boolean destroy = false;

        public ClickEvent(InventoryClickEvent event) {
            this.event = event;
        }

        public boolean isClose() {
            return close;
        }

        public void setClose(boolean close) {
            this.close = close;
        }

        public boolean isDestroy() {
            return destroy;
        }

        public void setDestroy(boolean destroy) {
            if (this.destroy && !this.close) {
                this.close = true;
            }
            this.destroy = destroy;
        }

        public InventoryClickEvent getEvent() {
            return event;
        }
    }

    class Builder {
        private final Plugin plugin;
        private Map<Integer, ItemStack>    stacks   = Maps.newHashMap();
        private Map<Integer, ClickHandler> handlers = Maps.newHashMap();
        private String  name;
        private Integer size;

        Builder(Plugin plugin) {
            Validate.notNull(plugin);

            this.plugin = plugin;
        }

        public Builder name(String name) {
            this.name = name;

            return this;
        }

        public Builder size(Integer size) {
            this.size = size;

            return this;
        }

        public Builder stack(Integer index, ItemStack stack) {
            this.stacks.put(index, stack);

            return this;
        }

        public Builder handler(Integer index, ClickHandler handler) {
            this.handlers.put(index, handler);

            return this;
        }

        public Menu build() {
            Validate.notNull(size);
            Validate.notEmpty(name);

            return new OglofusMenu(plugin, stacks, handlers, name, size);
        }

        class OglofusMenu implements Menu {
            private final Plugin plugin;
            private Map<Integer, ItemStack>    stacks   = Maps.newHashMap();
            private Map<Integer, ClickHandler> handlers = Maps.newHashMap();
            private String  name;
            private Integer size;

            OglofusMenu(Plugin plugin, Map<Integer, ItemStack> stacks, Map<Integer, ClickHandler> handlers,
                        String name, Integer size) {
                this.plugin = plugin;
                this.stacks = stacks;
                this.handlers = handlers;
                this.name = name;
                this.size = size;
            }

            @Override
            public String getName() {
                return name;
            }

            @Override
            public Integer getSize() {
                return size;
            }

            @Override
            public Map<Integer, ItemStack> getStacks() {
                return stacks;
            }

            @Override
            public Map<Integer, ClickHandler> getHandlers() {
                return handlers;
            }

            @Override
            public void registerStack(Integer index, ItemStack stack) {
                stacks.put(index, stack);
            }

            @Override
            public void registerHandler(Integer index, ClickHandler handler) {
                handlers.put(index, handler);
            }

            @Override
            public void removeStack(Integer index) {
                if (stacks.containsKey(index)) stacks.remove(index);
            }

            @Override
            public void removeHandler(Integer index) {
                if (handlers.containsKey(index)) handlers.remove(index);
            }

            @Override
            public Inventory createInventory(InventoryHolder holder) {
                Validate.notNull(holder);
                Inventory inventory = Bukkit.createInventory(holder, size);

                for (Map.Entry<Integer, ItemStack> entry : stacks.entrySet()) {
                    if (entry.getKey() < size) {
                        inventory.setItem(entry.getKey(), entry.getValue());
                    }
                }

                return inventory;
            }

            @Override
            public Inventory updateInventory(Inventory inventory) {
                inventory.clear();

                for (Map.Entry<Integer, ItemStack> entry : stacks.entrySet()) {
                    if (entry.getKey() < size) {
                        inventory.setItem(entry.getKey(), entry.getValue());
                    }
                }

                return inventory;
            }

            @Override
            public void destroy() {
                HandlerList.unregisterAll(this);
            }

            @EventHandler
            public void onClick(InventoryClickEvent event) {
                if (event.getInventory().getName().equals(name)) {
                    if (handlers.containsKey(event.getRawSlot())) {
                        ClickEvent handler = new ClickEvent(event);

                        handlers.get(event.getRawSlot()).execute(handler);

                        if (handler.isClose()) {
                            event.getWhoClicked().closeInventory();
                        }

                        if (handler.isDestroy()) {
                            destroy();
                        }
                    }
                }
            }
        }
    }
}
