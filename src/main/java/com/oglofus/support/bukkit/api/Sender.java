package com.oglofus.support.bukkit.api;

import com.sk89q.intake.parametric.annotation.Classifier;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * This file is part of Oglofus Support project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 04/06/2017.
 */
@Classifier
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER, ElementType.FIELD})
public @interface Sender {}
