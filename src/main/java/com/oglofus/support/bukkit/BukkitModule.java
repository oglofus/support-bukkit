package com.oglofus.support.bukkit;

import com.google.common.collect.Lists;
import com.oglofus.support.bukkit.api.Sender;
import com.sk89q.intake.argument.ArgumentException;
import com.sk89q.intake.argument.ArgumentParseException;
import com.sk89q.intake.argument.CommandArgs;
import com.sk89q.intake.argument.Namespace;
import com.sk89q.intake.parametric.AbstractModule;
import com.sk89q.intake.parametric.Provider;
import com.sk89q.intake.parametric.ProvisionException;
import com.sk89q.intake.parametric.provider.EnumProvider;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import org.jetbrains.annotations.Nullable;
import java.lang.annotation.Annotation;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * This file is part of Oglofus Support project.
 * Created by Nikolaos Grammatikos <nikosgram@protonmail.com> on 04/06/2017.
 */
public class BukkitModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(World.class).toProvider(new WorldProvider());
        bind(Player.class).annotatedWith(Sender.class).toProvider(new SenderPlayerProvider());
        bind(Player.class).toProvider(new PlayerProvider());
        bind(CommandSender.class).toProvider(new CommandSenderProvider());
        bind(OfflinePlayer.class).toProvider(new OfflinePlayerProvider());
        bind(Material.class).toProvider(new EnumProvider<>(Material.class));
    }

    public static class CommandSenderProvider implements Provider<CommandSender> {

        /**
         * Gets whether this provider does not actually consume values
         * from the argument stack and instead generates them otherwise.
         *
         * @return Whether values are provided without use of the arguments
         */
        @Override
        public boolean isProvided() {
            return true;
        }

        /**
         * Provide a value given the arguments.
         *
         * @param arguments The arguments
         * @param modifiers The modifiers on the parameter
         * @return The value provided
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Nullable
        @Override
        public CommandSender get(CommandArgs arguments, List<? extends Annotation> modifiers) throws
                ArgumentException, ProvisionException {
            Optional<CommandSender> sender = arguments.getNamespace().get(CommandSender.class);

            if (sender.isPresent()) {
                return sender.get();
            }

            throw new ProvisionException("The CommandSender is not setup.");
        }
    }

    public static class SenderPlayerProvider implements Provider<Player> {

        /**
         * Gets whether this provider does not actually consume values
         * from the argument stack and instead generates them otherwise.
         *
         * @return Whether values are provided without use of the arguments
         */
        @Override
        public boolean isProvided() {
            return true;
        }

        /**
         * Provide a value given the arguments.
         *
         * @param arguments The arguments
         * @param modifiers The modifiers on the parameter
         * @return The value provided
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Nullable
        @Override
        public Player get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException,
                ProvisionException {
            Optional<CommandSender> senderOptional = arguments.getNamespace().get(CommandSender.class);

            if (senderOptional.isPresent()) {
                CommandSender sender = senderOptional.get();

                if (sender instanceof Player) {
                    return (Player) sender;
                }
            }

            throw new ProvisionException("You must be a player to execute this command.");
        }
    }

    public static class PlayerProvider implements Provider<Player> {
        /**
         * Gets whether this provider does not actually consume values
         * from the argument stack and instead generates them otherwise.
         *
         * @return Whether values are provided without use of the arguments
         */
        @Override
        public boolean isProvided() {
            return false;
        }

        /**
         * Provide a value given the arguments.
         *
         * @param arguments The arguments
         * @param modifiers The modifiers on the parameter
         * @return The value provided
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Nullable
        @Override
        public Player get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException,
                ProvisionException {
            String name = arguments.next();

            Player target = Bukkit.getPlayer(name);

            if (target == null) {
                throw new ArgumentParseException("No celestial player by the name of '" + name + "' is known!");
            }

            return target;
        }

        /**
         * Get a list of suggestions for the given parameter and user arguments.
         * <p>
         * <p>If no suggestions could be enumerated, an empty list should
         * be returned.</p>
         *
         * @param namespace The namespace
         * @param modifiers The modifiers on the parameter
         * @param prefix
         * @return A list of suggestions
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Override
        public List<String> getSuggestions(Namespace namespace, List<? extends Annotation> modifiers, String prefix)
                throws ArgumentException, ProvisionException {
            return Bukkit.getOnlinePlayers()
                    .parallelStream()
                    .map(Player::getName)
                    .filter(name -> name.startsWith(prefix))
                    .collect(Collectors.toList());
        }
    }

    public static class OfflinePlayerProvider implements Provider<OfflinePlayer> {
        /**
         * Gets whether this provider does not actually consume values
         * from the argument stack and instead generates them otherwise.
         *
         * @return Whether values are provided without use of the arguments
         */
        @Override
        public boolean isProvided() {
            return false;
        }

        /**
         * Provide a value given the arguments.
         *
         * @param arguments The arguments
         * @param modifiers The modifiers on the parameter
         * @return The value provided
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Nullable
        @Override
        public OfflinePlayer get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException,
                ProvisionException {
            String name = arguments.next();

            OfflinePlayer target = Bukkit.getOfflinePlayer(name);

            if (target == null) {
                throw new ArgumentParseException("No celestial user by the name of '" + name + "' is known!");
            }

            return target;
        }

        /**
         * Get a list of suggestions for the given parameter and user arguments.
         * <p>
         * <p>If no suggestions could be enumerated, an empty list should
         * be returned.</p>
         *
         * @param namespace The namespace
         * @param modifiers The modifiers on the parameter
         * @param prefix
         * @return A list of suggestions
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Override
        public List<String> getSuggestions(Namespace namespace, List<? extends Annotation> modifiers, String prefix)
                throws ArgumentException, ProvisionException {
            return Lists.newArrayList(Bukkit.getOfflinePlayers())
                    .parallelStream()
                    .map(OfflinePlayer::getName)
                    .filter(name -> name.startsWith(prefix))
                    .collect(Collectors.toList());
        }
    }

    public static class WorldProvider implements Provider<World> {

        /**
         * Gets whether this provider does not actually consume values
         * from the argument stack and instead generates them otherwise.
         *
         * @return Whether values are provided without use of the arguments
         */
        @Override
        public boolean isProvided() {
            return false;
        }

        /**
         * Provide a value given the arguments.
         *
         * @param arguments The arguments
         * @param modifiers The modifiers on the parameter
         * @return The value provided
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Nullable
        @Override
        public World get(CommandArgs arguments, List<? extends Annotation> modifiers) throws ArgumentException,
                ProvisionException {
            String name = arguments.next();

            World target = Bukkit.getWorld(name);

            if (target == null) {
                throw new ArgumentParseException("No celestial world by the name of '" + name + "' is known!");
            }

            return target;
        }

        /**
         * Get a list of suggestions for the given parameter and user arguments.
         * <p>
         * <p>If no suggestions could be enumerated, an empty list should
         * be returned.</p>
         *
         * @param namespace The namespace
         * @param modifiers The modifiers on the parameter
         * @param prefix
         * @return A list of suggestions
         * @throws ArgumentException  If there is a problem with the argument
         * @throws ProvisionException If there is a problem with the provider
         */
        @Override
        public List<String> getSuggestions(Namespace namespace, List<? extends Annotation> modifiers, String prefix)
                throws ArgumentException, ProvisionException {

            return Bukkit.getWorlds()
                    .parallelStream()
                    .map(World::getName)
                    .filter(name -> name.startsWith(prefix))
                    .collect(Collectors.toList());
        }
    }
}
